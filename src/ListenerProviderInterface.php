<?php
/**
 * PHP FIG
 *
 * @category     PSR-14
 * @package      EventDispatcher
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\EventDispatcher;

/**
 * Mapper from an event to the listeners that are applicable to that event.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ListenerProviderInterface extends Psr\EventDispatcher\ListenerProviderInterface {
	
	/**
	 * @param    object    $event
	 *   An event for which to return the relevant listeners.
	 *
	 * @return iterable<callable>
	 *   An iterable (array, iterator, or generator) of callables.  Each
	 *   callable MUST be type-compatible with $event.
	 * @since   3.0.0 First time introduced.
	 */
	public function getListenersForEvent(object $event) : iterable;
}
