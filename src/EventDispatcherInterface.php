<?php
/**
 * PHP FIG
 *
 * @category     PSR-14
 * @package      EventDispatcher
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\EventDispatcher;

/**
 * Defines a dispatcher for events.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface EventDispatcherInterface extends Psr\EventDispatcher\EventDispatcherInterface {
	
	/**
	 * Provide all relevant listeners with an event to process.
	 *
	 * @param    object    $event
	 *   The object to process.
	 *
	 * @return object
	 *   The Event that was passed, now modified by listeners.
	 * @since   3.0.0 First time introduced.
	 */
	public function dispatch(object $event) : object;
}
