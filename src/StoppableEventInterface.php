<?php
/**
 * PHP FIG
 *
 * @category     PSR-14
 * @package      EventDispatcher
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\EventDispatcher;

/**
 * An Event whose processing may be interrupted when the event has been handled.
 * A Dispatcher implementation MUST check to determine if an Event
 * is marked as stopped after each listener is called.  If it is then it should
 * return immediately without calling any further Listeners.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface StoppableEventInterface extends Psr\EventDispatcher\StoppableEventInterface {
	
	/**
	 * Is propagation stopped?
	 * This will typically only be used by the Dispatcher to determine if the
	 * previous listener halted propagation.
	 *
	 * @return bool
	 *   True if the Event is complete and no further listeners should be called.
	 *   False to continue calling listeners.
	 * @since   3.0.0 First time introduced.
	 */
	public function isPropagationStopped() : bool;
}
